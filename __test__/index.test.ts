import { log, get, set, merge } from '../src';

console.trace = jest.fn();

describe('js-helpful', () => {
    it('log', () => {
        console.log('verb test');
        log.verb('verb test');
        console.log('info test', 'info test 2');
        log.info('info test', 'info test 2');
        console.log({ debug: 'test' }, 'debug test', { debug: 'test2' });
        log.debug({ debug: 'test' }, 'debug test', { debug: 'test2' });
        console.log('warn test', { warn: 'test' }, 'warn test3');
        log.warn('warn test', { warn: 'test' }, 'warn test3');

        console.groupCollapsed = undefined;
        console.log('error', 'test', { error: 'test2' }, { error: 'test3' });
        log.error('error', 'test', { error: 'test2' }, { error: 'test3' });
    });

    describe('values', () => {
        it('get, set', () => {
            const set_value = Math.random();
            const value = {};
            set(value, 'a', 'b', 'c', set_value);
            expect(get(value, 'a', 'b', 'c')).toEqual(set_value);
        });

        it('merge', () => {
            const merge_value = { a: 1, b: 2, c: 3, d: undefined };
            const dest_value = { a: 3, b: 2, c: undefined, d: 1 };
            const value = merge(merge_value, dest_value);
            expect(value.a).toEqual(dest_value.a);
            expect(value.c).toEqual(merge_value.c);
            expect(value.d).toEqual(dest_value.d);
        });
    });
});
