export function get(src_: any, ...keys_: string[]) {
    let ret = src_;
    if (ret == null)
        return null;
    for (const key of keys_) {
        ret = ret[key];
        if (ret == null)
            return null;
    }
    return ret;
}

export function set(dst_: any, ...src_: any[]): any {
    if (!dst_)
        return dst_;

    let tmp = dst_;
    for (let i = 0; i < src_.length - 1; i++) {
        const key = src_[i];
        if (!(key in tmp) || !tmp[key] || i === src_.length - 2) {
            tmp[key] = (!(key in tmp) || !tmp[key]) ? ((i !== src_.length - 2) ? {} : src_[src_.length - 1]) : src_[src_.length - 1];
        }

        tmp = tmp[key];
    }

    return dst_;
}

export function merge(...src_: any[]): any {
    if (src_ == null || src_.length === 0)
        return src_;

    const ret = Object.assign({}, src_[0]);
    for (let i = 1; i < src_.length; i++) {
        const src = src_[i];
        for (const key in src) {
            if (src[key] == null)
                continue;

            ret[key] = src[key];
        }
    }
    return ret;
}
