import log from './log';
import { get, set, merge } from './values';

export { log, get, set, merge };
