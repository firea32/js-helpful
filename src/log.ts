function _generate_args(css_: string, args_: any[]): { obj: any[], msg: string[] } {
    const ret = {
        obj: [],
        msg: undefined,
    };

    if (args_ != null) {
        ret.msg = '%c';
        for (let i = 0; i < args_.length; i++) {
            const arg = args_[i];
            let msg = arg;
            if (typeof arg === 'object') {
                ret.obj.push(arg);
                msg = `[${ ret.obj.length - 1 }]`;
            }
            ret.msg += msg + (i < args_.length - 1 ? ' ' : '');
        }
        ret.msg = [ret.msg, css_];
    }
    return ret;
}

function _console_log(args_: { obj: any[], msg: string[] }) {
    const { obj, msg } = args_;
    try {
        console.groupCollapsed.apply(console, msg);
        for (let i = 0; i < obj.length; i++) {
            console.log(i, obj[i]);
        }
        {
            console.groupCollapsed('Call Stack');
            console.trace();
            console.groupEnd();
        }
        console.groupEnd();
    } catch {
        console.log.apply(console, msg);
        for (let i = 0; i < obj.length; i++) {
            console.log(i, obj[i]);
        }
    }
}

export default class log {
    static enable: boolean = true;

    static verb(...args_: any[]) {
        if (!this.enable)
            return;

        try {
            console.groupCollapsed.apply(console, args_);
            console.trace();
            console.groupEnd();
        } catch {
            console.log.apply(console, args_);
        }
    }

    static info(...args_: any[]) {
        if (!this.enable)
            return;

        _console_log(_generate_args('color: black; background: #66C2CD', args_));
    }

    static debug(...args_: any[]) {
        if (!this.enable)
            return;

        _console_log(_generate_args('color: black; background: #A8CC8C', args_));
    }

    static warn(...args_: any[]) {
        if (!this.enable)
            return;

        _console_log(_generate_args('color: black; background: #DBAB79', args_));
    }

    static error(...args_: any[]) {
        if (!this.enable)
            return;

        _console_log(_generate_args('color: black; background: #E88388', args_));
    }
}
